<?php
/**
 * @package Map_Places
 * @version 1.0
 */
/*
Plugin Name: Map Places
Description: Plugin for creating places on the map.
Author: Ivan Kovbas
Version: 1.0
*/
global $wpdb;

define( 'MP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'MP_CSS_URL', MP_PLUGIN_URL . 'css/' );
define( 'MP_JS_URL', MP_PLUGIN_URL . 'js/' );
define( 'MP_IMG_URL', MP_PLUGIN_URL . 'img/' );
define( 'MP_MARKERS_URL', MP_IMG_URL . 'markers/' );

define( 'MP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'MP_VIEWS_DIR', MP_PLUGIN_DIR . 'views/' );
define( 'MP_IMG_DIR', MP_PLUGIN_DIR . 'img/' );
define( 'MP_MARKERS_DIR', MP_IMG_DIR . 'markers/' );

define( 'MP_TABLE_PREFIX', $wpdb->prefix . 'mp_' );
define( 'MP_TABLE_CATEGORIES', MP_TABLE_PREFIX . 'categories' );
define( 'MP_TABLE_PLACES', MP_TABLE_PREFIX . 'places' );
define( 'MP_TABLE_MARKERS', MP_TABLE_PREFIX . 'markers' );

require_once( MP_PLUGIN_DIR . 'class.simple-image.php' );
require_once( MP_PLUGIN_DIR . 'class.map-places.php' );
require_once( MP_PLUGIN_DIR . 'class.map-places-installer.php' );
require_once( MP_PLUGIN_DIR . 'class.map-places-admin.php' );
require_once( MP_PLUGIN_DIR . 'class.map-places-shortcode.php' );

MapPlacesInstaller::init(); // init installer
MapPlacesShortCode::init(); // init shortcode (client side part)
MapPlacesAdmin::init();
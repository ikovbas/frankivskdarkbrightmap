<div style="display: none;">
    <div id="create-marker-dialog" class="dialog-form create-marker-form" title="Створіть новий маркер">
        <form id="marker-form">
            <div class="input-group">
                <label for="name">Ім'я:</label>
                <input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all">
                <div class="error-container"></div>
            </div>
            <div class="input-group">
                <label for="description">Опис:</label>
                <textarea name="description" id="description" class="text ui-widget-content ui-corner-all"></textarea>
                <div class="error-container"></div>
            </div>
            <div class="input-group">
                <label for="marker_image">Завантажте ярлик:</label> <br>
                <input type="file" name="marker_image" id="marker_image" class="text ui-widget-content ui-corner-all">
                <div class="error-container"></div>
            </div>
            <div class="error-container"></div>
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </form>
    </div>
</div>
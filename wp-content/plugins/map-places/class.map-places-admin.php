<?php

/**
 * Class MapPlacesAdmin
 * @author Ivan Kovbas kovbas.ivan@gmail.com
 */
class MapPlacesAdmin {

    /**
     * Method for initialize installer
     */
    public static function init() {

        // Add plugin configuration menu to left panel
        add_action('admin_menu', function () {
            add_menu_page('Map Places Settings', 'Map Places', 'manage_options', 'map_places_settings',
                function() {}, MP_PLUGIN_URL . '/img/map-white-20-16.png', 2 );
        });
    }
}
var MpControlPanel = function (categories) {

    var navWrapper      = null;
    var controlGroups   = null;
    var accordion       = null;
    var showAllButton   = null;
    var addPlaceButton  = null;

    (function init() {
        navWrapper      = document.createElement('div');
        controlGroups   = document.createElement('div');
        accordion       = document.createElement('div');
        showAllButton   = document.createElement('input');
        addPlaceButton       = document.createElement('input');

        navWrapper.className    = 'mp-nav';
        controlGroups.className = 'control-groups';
        accordion.id            = 'accordion';

        showAllButton.className = 'left-button';
        showAllButton.type      = 'button';
        showAllButton.value     = 'сховати всі';

        addPlaceButton.className     = 'right-button';
        addPlaceButton.type          = 'button';
        addPlaceButton.value         = 'додати';

        navWrapper.appendChild(controlGroups);
        controlGroups.appendChild(showAllButton);
        controlGroups.appendChild(addPlaceButton);
        controlGroups.appendChild(accordion);

        categories.forEach(function (category) {
            var pair = category.getHtml();
            accordion.appendChild(pair[0]);
            accordion.appendChild(pair[1]);
        });
    })();

    return {

        /**
         * Should be called after adding element to dom
         */
        init : function () {
            jQuery( "#accordion" ).accordion({
                active: null,
                animate: 200,
                collapsible: true,
                heightStyle: 'content'
            });
        },

        onAddMarkerButtonClick : function (callback) {
            // Subscribe on event
            categories.forEach(function (category) {
                category.onAddMarkerButtonClick(callback);
            });
        },

        getHtml : function () { return navWrapper; }
    };
};
var Category = function (categoryData) {

    var id      = categoryData.id || null;
    var name    = categoryData.name;
    var markers = [];

    // create new dom elements
    var body            = document.createElement("div");
    var header          = document.createElement("h3");
    var markersWrapper  = document.createElement('ul');
    var addMarkerButton = document.createElement('a');
    var addButtonIcon   = document.createElement('span');

    var addMarkerByData = function (markerData) {
        var marker = new Marker(markerData);
        markers.push(marker); // add new marker to markers array
        markersWrapper.appendChild(marker.getHtml()); // add marker html to category body section
    };

    (function init() {

        // Initialize elements

        addButtonIcon.className   = 'add-button-icon';
        addButtonIcon.appendChild(document.createTextNode('+'));
        addMarkerButton.href        = '';

        addMarkerButton.onclick     = function (e) { e.preventDefault() };
        addMarkerButton.className   = 'add-marker-button';
        //addMarkerButton.appendChild(addButtonIcon);
        addMarkerButton.appendChild(document.createTextNode('Додати Новий Маркер'));

        // Add category name to category element
        header.appendChild(document.createTextNode(name));
        body.appendChild(markersWrapper);
        body.appendChild(addMarkerButton);

        // Create markers
        categoryData.markers.forEach(addMarkerByData);
    })();

    return {
        getId : function () { return id; },

        getName : function () { return name; },

        getMarkers : function () { return markers; },

        getHtml : function () { return [header, body]; },

        onAddMarkerButtonClick : function (callback) {
            var thiss = this;
            addMarkerButton.onclick = function (e) {
                e.preventDefault();
                callback(thiss);
            }
        },

        addMarkerByData : addMarkerByData
    };
};
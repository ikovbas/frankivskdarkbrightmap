var MpProgressBar = function () {

    var progressWrapper = jQuery( "#progress-wrapper" ).clone();
    jQuery("body").append(jQuery(progressWrapper));

    return {
        show : function () {
            progressWrapper.css({ display: 'block' });
        },
        hide : function () {
            progressWrapper.css({ display: 'none' });
        }
    };
};
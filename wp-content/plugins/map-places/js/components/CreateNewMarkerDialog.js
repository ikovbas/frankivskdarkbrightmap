var CreateNewMarkerDialog = function () {

    var onSuccess = function () {};
    var category = null;
    var errorContainer = jQuery('#create-marker-dialog .error-container').last();

    var showErrors = function (responce) {
        try {
            // Try to parse server response as json
            var data = JSON.parse(responce.responseText);
            if (data.message) {
                errorContainer.html(data.message).show();
            } else if (data.errors) {
                Object.keys(data.errors).forEach(function (field) {
                    jQuery('#marker-form #' + field).next('.error-container').html(data.errors[field].join('<br />')).show()
                });
            } else {
                errorContainer.html(responce.status + ': ' + responce.statusText).show();
                console.log(data);
            }
            // Trigger window resize to processed dialog window reposition
            jQuery(window).trigger('resize');
        } catch (e) {
            // show response status code and text if cant parse response as json
            errorContainer.html(responce.status + ': ' + responce.statusText).show();
        }
    };

    var clearErrors = function () {
        jQuery('#create-marker-dialog .error-container').html('').hide();
    };

    var createMarker = function (e) {
        if (e) {e.preventDefault();}
        clearErrors();
        document.mpProgressBar.show();

        var markerImage = jQuery('#marker-form #marker_image').prop("files")[0];
        var name        = jQuery('#marker-form #name').val();
        var description = jQuery('#marker-form #description').val();

        var form_data = new FormData();
        form_data.append("marker_image", markerImage);
        form_data.append("name", name);
        form_data.append("description", description);
        form_data.append("action", 'create_marker');
        form_data.append("category_id", category.id);

        jQuery.ajax({
            url: ajax_object.ajax_url,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post'
        }).done(function (response) {
            onSuccess(response.marker);
        }).fail(function(response) {
            showErrors(response);
        }).always(function () {
            document.mpProgressBar.hide();
        });
    };

    var dialog = jQuery( "#create-marker-dialog" ).dialog({
        autoOpen: false,
        resizable: false,
        width: 350,
        closeOnEscape: false,
        draggable: false,
        modal: true,
        buttons: {
            "first": {
                text: 'Відмінити',
                class: 'btn btn-default',
                click: function() { dialog.dialog( "close" ); }
            },
            "second": {
                text: 'Зберегти',
                class: 'btn btn-primary',
                click: createMarker
            }
        },
        open: function(event, ui) {
            jQuery(event.target).dialog('widget')
                .css({ position: 'fixed' })
                .position({ my: 'center', at: 'center', of: window });

            jQuery(window).resize(function() {
                jQuery(event.target).dialog('widget')
                    .position({ my: 'center', at: 'center', of: window });
            });
        },
        close: function() {
            //form[ 0 ].reset();
            //allFields.removeClass( "ui-state-error" );
            clearErrors();
        }
    });

    jQuery("#create-marker-dialog form").submit(createMarker);

    return {
        open : function (tmpCategory, callback) {
            category = tmpCategory;
            onSuccess = callback;
            dialog.dialog( "open" );
        },
        close : function () {
            dialog.dialog( "close" );
        }
    };
};

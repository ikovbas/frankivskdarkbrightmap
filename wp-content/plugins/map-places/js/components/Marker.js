var Marker = function (markerData) {

    var id          = markerData.id || null;
    var name        = markerData.name;
    var description = markerData.description;
    var places      = [];
    var closed      = true;

    var li = null;
    var a = null;
    var openButton = document.createElement("div");
    var openButtonIcon  = document.createElement("i");

    var remove = function () {
        document.mpProgressBar.show();

        jQuery.post(ajax_object.ajax_url, {action : 'remove_marker', marker_id : id}, 'json')
            .done(function (response) {
                alert(response.message);
                console.log(response);
            }).fail(function(response) {
                showError(response);
            }).always(function () {
                document.mpProgressBar.hide();
            });
    };

    var open = function () {
        //a.style.marginLeft = '100px';
        //openButtonIcon.className    += 'fa fa-chevron-left';
        //closed = false;

        remove();
    };

    var close = function () {
        a.style.marginLeft = '0px';
        openButtonIcon.className    += 'fa fa-chevron-right';
        closed = true;
    };

    var arrowClick = function (e) {
        e.preventDefault();
        if (closed) { open(); } else { close(); }
    };

    var elementClick = function () {
//        alert('ok');
    };

    var showError = function (responce) {
        try {
            // Try to parse server response as json
            var data = JSON.parse(responce.responseText);
            if (data.message) {
                alert(data.message);
            } else {
                alert(responce.status + ': ' + responce.statusText);
                console.log(data);
            }
        } catch (e) {
            // show response status code and text if cant parse response as json
            alert(responce.status + ': ' + responce.statusText);
        }
    };

    (function init() {
        // create new dom elements
        li    = document.createElement("li");
        a       = document.createElement("a");
        var img = document.createElement("img");
        var titleWrapper        = document.createElement("div");
        var descriptionWrapper  = document.createElement("div");

        // Create table
        var table   = document.createElement("table");
        var tr      = document.createElement("tr");
        var markerControlsWrapper  = document.createElement("td");
        var textWrapper     = document.createElement("td");
        var imageWrapper    = document.createElement("td");
        openButtonIcon  = document.createElement("i");

        a.href="#";
        a.addEventListener("click", elementClick);

        img.src = markerData.url;

        textWrapper.className   += 'text-wrapper';
        imageWrapper.className  += 'image-wrapper';

        titleWrapper.className  += 'title-wrapper';
        titleWrapper.title      = name;

        descriptionWrapper.className    += 'description-wrapper';
        descriptionWrapper.title        += description;

        markerControlsWrapper.className += 'marker-controls-wrapper';
        markerControlsWrapper.addEventListener("click", arrowClick);
        openButton.className        += 'open-button';
        openButtonIcon.className    += 'fa fa-chevron-right';

        // Add marker name to marker element

        li.appendChild(a);
        a.appendChild(table);
        table.appendChild(tr);

        tr.appendChild(markerControlsWrapper);
        markerControlsWrapper.appendChild(openButton);
        openButton.appendChild(openButtonIcon);

        tr.appendChild(imageWrapper);
        imageWrapper.appendChild(img);

        tr.appendChild(textWrapper);
        textWrapper.appendChild(titleWrapper);
        textWrapper.appendChild(descriptionWrapper);
        titleWrapper.appendChild(document.createTextNode(name));
        descriptionWrapper.appendChild(document.createTextNode(description));

        // set places
        places = markerData.places || [];
    })();

    return {
        getId : function () { return id; },

        getName : function () { return name; },

        getPlaces : function () { return places; },

        getHtml : function () { return li; }
    };
};
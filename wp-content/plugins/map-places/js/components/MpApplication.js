var MpApplication = function (wrapper) {

    var map             = new MpMap();
    var controlPanel    = null;
    var categories      = [];

    var createNewMarkerDialog = null;

    return {
        init : function (initData) {

            if (!wrapper) {return;}

            categories = initData.categories.map(function (categoryData) {
                return new Category(categoryData);
            });

            /** Create elements */
            // Create control panel
            controlPanel = new MpControlPanel(categories);

            // Create createNewMarkerDialog
            createNewMarkerDialog = new CreateNewMarkerDialog();

            /** Add elements to dom(bind to each other) and initialize them */
            // Add map to dom and initialize
            wrapper.appendChild(map.getHtml());
            map.init();

            // Add control panel and initialize accordion
            wrapper.appendChild(controlPanel.getHtml());
            controlPanel.init();

            /** Subscribe on events */
            // Add an event on click on AddMarkerButton
            controlPanel.onAddMarkerButtonClick(function (category) {
                createNewMarkerDialog.open(category, function (markerData) {
                    // Add marker to category on successfully creating
                    category.addMarkerByData(markerData);
                    createNewMarkerDialog.close();
                });
            });
        }
    };
};
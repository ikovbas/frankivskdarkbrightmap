var MpMap = function () {

    var mapWrapper = null;
    var mapElement = null;

    var mapUrl = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
    var startX = 48.92278;
    var startY = 24.71035;
    var startZ = 13;

    (function init() {
        mapWrapper = document.createElement('div');
        mapElement = document.createElement('div');

        mapWrapper.className = 'mp-map';
        mapElement.className = 'map';

        mapWrapper.appendChild(mapElement);
    })();

    return {

        /**
         * Should be called after adding element to dom
         */
        init : function () {
            var map = L.map(mapElement).setView([startX, startY], startZ);
            L.tileLayer(mapUrl).addTo(map);
        },

        getHtml : function () { return mapWrapper; }
    };
};
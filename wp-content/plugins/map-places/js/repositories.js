/**
 * Created by ikovbas on 17.04.16.
 */
var mapPlacesRepositories = angular.module('mapPlacesRepositories', ['ngResource']);

mapPlacesRepositories.factory('Repository', ['$q', '$http', 'API_PATH',
    function($q, $http, API_PATH){

        var Repository = {};

        Repository.getInitData = function () {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: API_PATH,
                params: {
                    action: 'get_init_data',
                }
            }).success(function (result) {
                deferred.resolve(result);
            }).error(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return Repository;
    }
]);
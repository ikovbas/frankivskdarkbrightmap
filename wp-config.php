<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fdb_map');

/** MySQL database username */
define('DB_USER', 'fdb_map');

/** MySQL database password */
define('DB_PASSWORD', 'e&:~URf2H<}[G2BZ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%h>1bt+gdm+l3r7%E4D;E#1BV#<)CPJ=zK(2<Z5x=4 k;^kW$u.37xC[-)|m!]: ');
define('SECURE_AUTH_KEY',  ']6RK&eUtlqY)3NO0T^n+<=_L(/Zy.m;hngr|N?}>GhW~s^c}j1aU_;ZPu9=kat`V');
define('LOGGED_IN_KEY',    'SWT8{h4JMyqOG:uVh |-ZdQ+L{EZ|]Q}o0%Iq_il}K#k@0?7L2q_{)6c=#95s1rs');
define('NONCE_KEY',        '[jWc-(d*qpqKYj=/Bc$wg!1jtQ36-aY:@JE$l)oKD.c,lTq{?Ks=6Wfy#J-}{ed4');
define('AUTH_SALT',        '=-~Is/q-D9I?q><u,%3hbFm9{60B,w91Cq7>tv4r &k<8oB#tM6x{zv%{7l|x(%H');
define('SECURE_AUTH_SALT', 'b~cf~t#2GCz4kBDM@F*$=0~r4bA}=@$k+SLpWf@)Zj|dn!tk$x&6mi%kupCYFfiN');
define('LOGGED_IN_SALT',   '68<]+Z}+t$9^c~3kkNcpMd`S^$EuZ!C|Plu1(2QddmV4:hn]c0>0)p~t6O%a/>dl');
define('NONCE_SALT',       '~uN-x&a_Y<N1nOvb|YQ?&R?hCd2n/(@>C8sxkd5C6P2]|-Uixgyh(BCX*-c94zPM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
